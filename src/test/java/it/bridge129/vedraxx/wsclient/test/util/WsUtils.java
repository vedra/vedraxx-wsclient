package it.bridge129.vedraxx.wsclient.test.util;

import org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO.TransitNewDTO;
import org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO.TransitResourceDTO;
import org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ClassVehicleEnum;
import org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.DirectionEnum;
import org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.NationalityEnum;
import org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ResourceExtensionEnum;
import org.tempuri.ITransitiService;
import org.tempuri.TransitServiceLocator;

import javax.xml.rpc.ServiceException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class WsUtils {

    public static ITransitiService service(String url) throws ServiceException, MalformedURLException {
        TransitServiceLocator locator = new TransitServiceLocator();
        ITransitiService service = locator.getBasicHttpBinding_ITransitiService(new URL(url));
        return service;
    }

    /*
    Uuid|
    Numerazione|
    TipoVarco|
    CodiceApparecchio|
    DescrizioneVarco|
    MatricolaApparecchio|
    Data|6
    Ora|7
    Targa|8
    Classe|
    NazionalitaVeicolo|
    DataConferma|
    OrarioConferma|
    IdentificativoOperatoreConferma|
    NomeCognomeOperatoreConferma|
    Immagine1 15
     */
    public static Integer send(String line, String folder, ITransitiService service) throws Exception {
        String[] fields = line.split("\\|");
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        TransitNewDTO transit = new TransitNewDTO();
        transit.setConfidence(5);
        transit.setGateSignature("a877b800-fcb9-4832-9bc6-36b8cd719e12");
        Calendar calendar = Calendar.getInstance();
//      Date date = sf.parse(fields[6] + " " + fields[7]);
//      calendar.setTime(date);

        transit.setCaputureDateAndTimeUTC(calendar);
        transit.setClassVeichle(ClassVehicleEnum.Autocarro);
        transit.setDirection(DirectionEnum.In);
        transit.setPlate(fields[8]);
        transit.setNationlity(NationalityEnum.IT);
        TransitResourceDTO[] transitResourceDTOS = new TransitResourceDTO[1];
        byte[] jpdata = FileUtils
                .getBytesFromFile(new File(folder + fields[15]));
        transitResourceDTOS[0] = new TransitResourceDTO();
        transitResourceDTOS[0].setData(jpdata);
        transitResourceDTOS[0].setExtension(ResourceExtensionEnum.JPG);
        transitResourceDTOS[0].setId(1);
        transitResourceDTOS[0].setName(fields[15]);
        transit.setResources(transitResourceDTOS);
        Integer result = service.createTransit(transit);
        return result;
    }

    public static Integer sendDirect(String gate, String plateNumber, String file, String immagine,
                                     Instant instant, ITransitiService service) throws Exception {
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        TransitNewDTO transit = new TransitNewDTO();
        transit.setConfidence(5);
        transit.setGateSignature(gate);
        Calendar calendar = Calendar.getInstance();
//      Date date = sf.parse(fields[6] + " " + fields[7]);
        calendar.setTime(Date.from(instant));

        transit.setCaputureDateAndTimeUTC(calendar);
        transit.setClassVeichle(ClassVehicleEnum.Autocarro);
        transit.setDirection(DirectionEnum.In);
        transit.setPlate(plateNumber);
        transit.setNationlity(NationalityEnum.IT);
        TransitResourceDTO[] transitResourceDTOS = new TransitResourceDTO[1];
        byte[] jpdata = FileUtils
                .getBytesFromFile(new File(file));
        transitResourceDTOS[0] = new TransitResourceDTO();
        transitResourceDTOS[0].setData(jpdata);
        transitResourceDTOS[0].setExtension(ResourceExtensionEnum.JPG);
        transitResourceDTOS[0].setId(1);
        transitResourceDTOS[0].setName(immagine);
        transit.setResources(transitResourceDTOS);
        Integer result = service.createTransit(transit);
        return result;
    }
}
