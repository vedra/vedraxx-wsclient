package it.bridge129.vedraxx.wsclient.test;

import it.bridge129.vedraxx.wsclient.test.util.WsUtils;
import org.junit.Test;
import org.tempuri.ITransitiService;

import java.time.Instant;

public class SingleSenderWs {

    String immagine = "791afa7c-a211-4187-8b32-e05a6021f25f_1.jpg";
    String file = "docs/" + immagine;
    String gate = "a877b800-fcb9-4832-9bc6-36b8cd719e12";
    String plateNumber = "EX610LW";
    Instant instant = Instant.now();
    String url = "http://localhost:8097/soap/BasicHttpBinding_ITransitiService";

    @Test
    public void send() throws Exception {
        ITransitiService service = WsUtils.service(url);
        Integer ig = WsUtils.sendDirect(gate, plateNumber, file, immagine, instant, service);
        System.out.println(ig);
    }
}
