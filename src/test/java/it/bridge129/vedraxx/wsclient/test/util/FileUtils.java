package it.bridge129.vedraxx.wsclient.test.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * @author fiorenzo pizza
 */
public class FileUtils {

    static Logger logger = Logger.getLogger(FileUtils.class.getCanonicalName());
    static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.

    public static List<String> readLinesFromTextFile(String fileName,
                                                     String encoding) {
        logger.info("Reading from file named " + fileName);
        Scanner scanner = null;
        List<String> result = new ArrayList<String>();
        try {
            if (encoding == null) {
                scanner = new Scanner(new File(fileName));
            } else {
                scanner = new Scanner(new File(fileName), encoding);
            }
            while (scanner.hasNextLine()) {
                result.add(scanner.nextLine());
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        } finally {
            scanner.close();
        }
        return result;
    }

    public static void writeBytesToFile(String path, byte[] bytes) throws Exception {
        FileOutputStream stream = new FileOutputStream(path);
        try {
            stream.write(bytes);
        } finally {
            stream.close();
        }
    }


    public static byte[] getBytesFromFile(File file) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            // Get the size of the file
            long length = file.length();
            if (length > Integer.MAX_VALUE) {
                // File is too large
                throw new IOException("File is too large: " + file.getName());
            }
            // Create the byte array to hold the data
            byte[] bytes = new byte[(int) length];
            // Read in the bytes
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length
                    && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
            // Ensure all the bytes have been read in
            if (offset < bytes.length) {
                throw new IOException("Could not completely read file "
                        + file.getName());
            }
            // Close the input stream and return bytes
            return bytes;
        } catch (Exception e) {
            logger.info(e.getMessage());
            return null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Throwable t) {
                }
            }
        }
    }


}
