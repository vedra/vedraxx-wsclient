/**
 * TransitResourceDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO;

public class TransitResourceDTO  implements java.io.Serializable {
    private byte[] data;

    private org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ResourceExtensionEnum extension;

    private Integer id;

    private String name;

    private String path;

    public TransitResourceDTO() {
    }

    public TransitResourceDTO(
           byte[] data,
           org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ResourceExtensionEnum extension,
           Integer id,
           String name,
           String path) {
           this.data = data;
           this.extension = extension;
           this.id = id;
           this.name = name;
           this.path = path;
    }


    /**
     * Gets the data value for this TransitResourceDTO.
     *
     * @return data
     */
    public byte[] getData() {
        return data;
    }


    /**
     * Sets the data value for this TransitResourceDTO.
     *
     * @param data
     */
    public void setData(byte[] data) {
        this.data = data;
    }


    /**
     * Gets the extension value for this TransitResourceDTO.
     *
     * @return extension
     */
    public org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ResourceExtensionEnum getExtension() {
        return extension;
    }


    /**
     * Sets the extension value for this TransitResourceDTO.
     *
     * @param extension
     */
    public void setExtension(org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ResourceExtensionEnum extension) {
        this.extension = extension;
    }


    /**
     * Gets the id value for this TransitResourceDTO.
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }


    /**
     * Sets the id value for this TransitResourceDTO.
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * Gets the name value for this TransitResourceDTO.
     *
     * @return name
     */
    public String getName() {
        return name;
    }


    /**
     * Sets the name value for this TransitResourceDTO.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Gets the path value for this TransitResourceDTO.
     *
     * @return path
     */
    public String getPath() {
        return path;
    }


    /**
     * Sets the path value for this TransitResourceDTO.
     *
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof TransitResourceDTO)) return false;
        TransitResourceDTO other = (TransitResourceDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.data==null && other.getData()==null) ||
             (this.data!=null &&
              java.util.Arrays.equals(this.data, other.getData()))) &&
            ((this.extension==null && other.getExtension()==null) ||
             (this.extension!=null &&
              this.extension.equals(other.getExtension()))) &&
            ((this.id==null && other.getId()==null) ||
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.name==null && other.getName()==null) ||
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.path==null && other.getPath()==null) ||
             (this.path!=null &&
              this.path.equals(other.getPath())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getData());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getExtension() != null) {
            _hashCode += getExtension().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getPath() != null) {
            _hashCode += getPath().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransitResourceDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "TransitResourceDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extension");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Extension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.Enums", "ResourceExtensionEnum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("path");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Path"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
