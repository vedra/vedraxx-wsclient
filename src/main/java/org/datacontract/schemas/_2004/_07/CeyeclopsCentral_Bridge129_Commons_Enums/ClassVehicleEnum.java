/**
 * ClassVehicleEnum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums;

public class ClassVehicleEnum implements java.io.Serializable {
    private String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ClassVehicleEnum(String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final String _NotDefine = "NotDefine";
    public static final String _Autoveicolo = "Autoveicolo";
    public static final String _Ciclomotore = "Ciclomotore";
    public static final String _Motociclo = "Motociclo";
    public static final String _Autocarro = "Autocarro";
    public static final String _Autobus = "Autobus";
    public static final ClassVehicleEnum NotDefine = new ClassVehicleEnum(_NotDefine);
    public static final ClassVehicleEnum Autoveicolo = new ClassVehicleEnum(_Autoveicolo);
    public static final ClassVehicleEnum Ciclomotore = new ClassVehicleEnum(_Ciclomotore);
    public static final ClassVehicleEnum Motociclo = new ClassVehicleEnum(_Motociclo);
    public static final ClassVehicleEnum Autocarro = new ClassVehicleEnum(_Autocarro);
    public static final ClassVehicleEnum Autobus = new ClassVehicleEnum(_Autobus);
    public String getValue() { return _value_;}
    public static ClassVehicleEnum fromValue(String value)
          throws IllegalArgumentException {
        ClassVehicleEnum enumeration = (ClassVehicleEnum)
            _table_.get(value);
        if (enumeration==null) throw new IllegalArgumentException();
        return enumeration;
    }
    public static ClassVehicleEnum fromString(String value)
          throws IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public String toString() { return _value_;}
    public Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClassVehicleEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.Enums", "ClassVehicleEnum"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
