/**
 * TransitNewDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO;

public class TransitNewDTO  implements java.io.Serializable {
    private java.util.Calendar caputureDateAndTimeUTC;

    private org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ClassVehicleEnum classVeichle;

    private Integer confidence;

    private org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.DirectionEnum direction;

    private String gateSignature;

    private org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.NationalityEnum nationlity;

    private String plate;

    private org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO.TransitResourceDTO[] resources;

    public TransitNewDTO() {
    }

    public TransitNewDTO(
           java.util.Calendar caputureDateAndTimeUTC,
           org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ClassVehicleEnum classVeichle,
           Integer confidence,
           org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.DirectionEnum direction,
           String gateSignature,
           org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.NationalityEnum nationlity,
           String plate,
           org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO.TransitResourceDTO[] resources) {
           this.caputureDateAndTimeUTC = caputureDateAndTimeUTC;
           this.classVeichle = classVeichle;
           this.confidence = confidence;
           this.direction = direction;
           this.gateSignature = gateSignature;
           this.nationlity = nationlity;
           this.plate = plate;
           this.resources = resources;
    }


    /**
     * Gets the caputureDateAndTimeUTC value for this TransitNewDTO.
     *
     * @return caputureDateAndTimeUTC
     */
    public java.util.Calendar getCaputureDateAndTimeUTC() {
        return caputureDateAndTimeUTC;
    }


    /**
     * Sets the caputureDateAndTimeUTC value for this TransitNewDTO.
     *
     * @param caputureDateAndTimeUTC
     */
    public void setCaputureDateAndTimeUTC(java.util.Calendar caputureDateAndTimeUTC) {
        this.caputureDateAndTimeUTC = caputureDateAndTimeUTC;
    }


    /**
     * Gets the classVeichle value for this TransitNewDTO.
     *
     * @return classVeichle
     */
    public org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ClassVehicleEnum getClassVeichle() {
        return classVeichle;
    }


    /**
     * Sets the classVeichle value for this TransitNewDTO.
     *
     * @param classVeichle
     */
    public void setClassVeichle(org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.ClassVehicleEnum classVeichle) {
        this.classVeichle = classVeichle;
    }


    /**
     * Gets the confidence value for this TransitNewDTO.
     *
     * @return confidence
     */
    public Integer getConfidence() {
        return confidence;
    }


    /**
     * Sets the confidence value for this TransitNewDTO.
     *
     * @param confidence
     */
    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }


    /**
     * Gets the direction value for this TransitNewDTO.
     *
     * @return direction
     */
    public org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.DirectionEnum getDirection() {
        return direction;
    }


    /**
     * Sets the direction value for this TransitNewDTO.
     *
     * @param direction
     */
    public void setDirection(org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.DirectionEnum direction) {
        this.direction = direction;
    }


    /**
     * Gets the gateSignature value for this TransitNewDTO.
     *
     * @return gateSignature
     */
    public String getGateSignature() {
        return gateSignature;
    }


    /**
     * Sets the gateSignature value for this TransitNewDTO.
     *
     * @param gateSignature
     */
    public void setGateSignature(String gateSignature) {
        this.gateSignature = gateSignature;
    }


    /**
     * Gets the nationlity value for this TransitNewDTO.
     *
     * @return nationlity
     */
    public org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.NationalityEnum getNationlity() {
        return nationlity;
    }


    /**
     * Sets the nationlity value for this TransitNewDTO.
     *
     * @param nationlity
     */
    public void setNationlity(org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums.NationalityEnum nationlity) {
        this.nationlity = nationlity;
    }


    /**
     * Gets the plate value for this TransitNewDTO.
     *
     * @return plate
     */
    public String getPlate() {
        return plate;
    }


    /**
     * Sets the plate value for this TransitNewDTO.
     *
     * @param plate
     */
    public void setPlate(String plate) {
        this.plate = plate;
    }


    /**
     * Gets the resources value for this TransitNewDTO.
     *
     * @return resources
     */
    public org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO.TransitResourceDTO[] getResources() {
        return resources;
    }


    /**
     * Sets the resources value for this TransitNewDTO.
     *
     * @param resources
     */
    public void setResources(org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO.TransitResourceDTO[] resources) {
        this.resources = resources;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof TransitNewDTO)) return false;
        TransitNewDTO other = (TransitNewDTO) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.caputureDateAndTimeUTC==null && other.getCaputureDateAndTimeUTC()==null) ||
             (this.caputureDateAndTimeUTC!=null &&
              this.caputureDateAndTimeUTC.equals(other.getCaputureDateAndTimeUTC()))) &&
            ((this.classVeichle==null && other.getClassVeichle()==null) ||
             (this.classVeichle!=null &&
              this.classVeichle.equals(other.getClassVeichle()))) &&
            ((this.confidence==null && other.getConfidence()==null) ||
             (this.confidence!=null &&
              this.confidence.equals(other.getConfidence()))) &&
            ((this.direction==null && other.getDirection()==null) ||
             (this.direction!=null &&
              this.direction.equals(other.getDirection()))) &&
            ((this.gateSignature==null && other.getGateSignature()==null) ||
             (this.gateSignature!=null &&
              this.gateSignature.equals(other.getGateSignature()))) &&
            ((this.nationlity==null && other.getNationlity()==null) ||
             (this.nationlity!=null &&
              this.nationlity.equals(other.getNationlity()))) &&
            ((this.plate==null && other.getPlate()==null) ||
             (this.plate!=null &&
              this.plate.equals(other.getPlate()))) &&
            ((this.resources==null && other.getResources()==null) ||
             (this.resources!=null &&
              java.util.Arrays.equals(this.resources, other.getResources())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCaputureDateAndTimeUTC() != null) {
            _hashCode += getCaputureDateAndTimeUTC().hashCode();
        }
        if (getClassVeichle() != null) {
            _hashCode += getClassVeichle().hashCode();
        }
        if (getConfidence() != null) {
            _hashCode += getConfidence().hashCode();
        }
        if (getDirection() != null) {
            _hashCode += getDirection().hashCode();
        }
        if (getGateSignature() != null) {
            _hashCode += getGateSignature().hashCode();
        }
        if (getNationlity() != null) {
            _hashCode += getNationlity().hashCode();
        }
        if (getPlate() != null) {
            _hashCode += getPlate().hashCode();
        }
        if (getResources() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResources());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getResources(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransitNewDTO.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "TransitNewDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("caputureDateAndTimeUTC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "CaputureDateAndTimeUTC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classVeichle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "ClassVeichle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.Enums", "ClassVehicleEnum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("confidence");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Confidence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("direction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Direction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.Enums", "DirectionEnum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gateSignature");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "GateSignature"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nationlity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Nationlity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.Enums", "NationalityEnum"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("plate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Plate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resources");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "Resources"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "TransitResourceDTO"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.DTO", "TransitResourceDTO"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
