/**
 * NationalityEnum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_Enums;

public class NationalityEnum implements java.io.Serializable {
    private String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected NationalityEnum(String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final String _UNKNOWN = "UNKNOWN";
    public static final String _AL = "AL";
    public static final String _AD = "AD";
    public static final String _AR = "AR";
    public static final String _AM = "AM";
    public static final String _AU = "AU";
    public static final String _AT = "AT";
    public static final String _AZ = "AZ";
    public static final String _BE = "BE";
    public static final String _BD = "BD";
    public static final String _BG = "BG";
    public static final String _BA = "BA";
    public static final String _BY = "BY";
    public static final String _BR = "BR";
    public static final String _CA = "CA";
    public static final String _CH = "CH";
    public static final String _CN = "CN";
    public static final String _CY = "CY";
    public static final String _CZ = "CZ";
    public static final String _DE = "DE";
    public static final String _DK = "DK";
    public static final String _DZ = "DZ";
    public static final String _EG = "EG";
    public static final String _ES = "ES";
    public static final String _EE = "EE";
    public static final String _ET = "ET";
    public static final String _FI = "FI";
    public static final String _FR = "FR";
    public static final String _GB = "GB";
    public static final String _GE = "GE";
    public static final String _GR = "GR";
    public static final String _HK = "HK";
    public static final String _HR = "HR";
    public static final String _HU = "HU";
    public static final String _IN = "IN";
    public static final String _IE = "IE";
    public static final String _IR = "IR";
    public static final String _IQ = "IQ";
    public static final String _IS = "IS";
    public static final String _IL = "IL";
    public static final String _IT = "IT";
    public static final String _JP = "JP";
    public static final String _KZ = "KZ";
    public static final String _KR = "KR";
    public static final String _LB = "LB";
    public static final String _LI = "LI";
    public static final String _LT = "LT";
    public static final String _LU = "LU";
    public static final String _LV = "LV";
    public static final String _MA = "MA";
    public static final String _MC = "MC";
    public static final String _MD = "MD";
    public static final String _MK = "MK";
    public static final String _MT = "MT";
    public static final String _NL = "NL";
    public static final String _NO = "NO";
    public static final String _PL = "PL";
    public static final String _PT = "PT";
    public static final String _RO = "RO";
    public static final String _RU = "RU";
    public static final String _RS = "RS";
    public static final String _ME = "ME";
    public static final String _SG = "SG";
    public static final String _SM = "SM";
    public static final String _SK = "SK";
    public static final String _SI = "SI";
    public static final String _SE = "SE";
    public static final String _SY = "SY";
    public static final String _TN = "TN";
    public static final String _TR = "TR";
    public static final String _UA = "UA";
    public static final String _US = "US";
    public static final String _VA = "VA";
    public static final NationalityEnum UNKNOWN = new NationalityEnum(_UNKNOWN);
    public static final NationalityEnum AL = new NationalityEnum(_AL);
    public static final NationalityEnum AD = new NationalityEnum(_AD);
    public static final NationalityEnum AR = new NationalityEnum(_AR);
    public static final NationalityEnum AM = new NationalityEnum(_AM);
    public static final NationalityEnum AU = new NationalityEnum(_AU);
    public static final NationalityEnum AT = new NationalityEnum(_AT);
    public static final NationalityEnum AZ = new NationalityEnum(_AZ);
    public static final NationalityEnum BE = new NationalityEnum(_BE);
    public static final NationalityEnum BD = new NationalityEnum(_BD);
    public static final NationalityEnum BG = new NationalityEnum(_BG);
    public static final NationalityEnum BA = new NationalityEnum(_BA);
    public static final NationalityEnum BY = new NationalityEnum(_BY);
    public static final NationalityEnum BR = new NationalityEnum(_BR);
    public static final NationalityEnum CA = new NationalityEnum(_CA);
    public static final NationalityEnum CH = new NationalityEnum(_CH);
    public static final NationalityEnum CN = new NationalityEnum(_CN);
    public static final NationalityEnum CY = new NationalityEnum(_CY);
    public static final NationalityEnum CZ = new NationalityEnum(_CZ);
    public static final NationalityEnum DE = new NationalityEnum(_DE);
    public static final NationalityEnum DK = new NationalityEnum(_DK);
    public static final NationalityEnum DZ = new NationalityEnum(_DZ);
    public static final NationalityEnum EG = new NationalityEnum(_EG);
    public static final NationalityEnum ES = new NationalityEnum(_ES);
    public static final NationalityEnum EE = new NationalityEnum(_EE);
    public static final NationalityEnum ET = new NationalityEnum(_ET);
    public static final NationalityEnum FI = new NationalityEnum(_FI);
    public static final NationalityEnum FR = new NationalityEnum(_FR);
    public static final NationalityEnum GB = new NationalityEnum(_GB);
    public static final NationalityEnum GE = new NationalityEnum(_GE);
    public static final NationalityEnum GR = new NationalityEnum(_GR);
    public static final NationalityEnum HK = new NationalityEnum(_HK);
    public static final NationalityEnum HR = new NationalityEnum(_HR);
    public static final NationalityEnum HU = new NationalityEnum(_HU);
    public static final NationalityEnum IN = new NationalityEnum(_IN);
    public static final NationalityEnum IE = new NationalityEnum(_IE);
    public static final NationalityEnum IR = new NationalityEnum(_IR);
    public static final NationalityEnum IQ = new NationalityEnum(_IQ);
    public static final NationalityEnum IS = new NationalityEnum(_IS);
    public static final NationalityEnum IL = new NationalityEnum(_IL);
    public static final NationalityEnum IT = new NationalityEnum(_IT);
    public static final NationalityEnum JP = new NationalityEnum(_JP);
    public static final NationalityEnum KZ = new NationalityEnum(_KZ);
    public static final NationalityEnum KR = new NationalityEnum(_KR);
    public static final NationalityEnum LB = new NationalityEnum(_LB);
    public static final NationalityEnum LI = new NationalityEnum(_LI);
    public static final NationalityEnum LT = new NationalityEnum(_LT);
    public static final NationalityEnum LU = new NationalityEnum(_LU);
    public static final NationalityEnum LV = new NationalityEnum(_LV);
    public static final NationalityEnum MA = new NationalityEnum(_MA);
    public static final NationalityEnum MC = new NationalityEnum(_MC);
    public static final NationalityEnum MD = new NationalityEnum(_MD);
    public static final NationalityEnum MK = new NationalityEnum(_MK);
    public static final NationalityEnum MT = new NationalityEnum(_MT);
    public static final NationalityEnum NL = new NationalityEnum(_NL);
    public static final NationalityEnum NO = new NationalityEnum(_NO);
    public static final NationalityEnum PL = new NationalityEnum(_PL);
    public static final NationalityEnum PT = new NationalityEnum(_PT);
    public static final NationalityEnum RO = new NationalityEnum(_RO);
    public static final NationalityEnum RU = new NationalityEnum(_RU);
    public static final NationalityEnum RS = new NationalityEnum(_RS);
    public static final NationalityEnum ME = new NationalityEnum(_ME);
    public static final NationalityEnum SG = new NationalityEnum(_SG);
    public static final NationalityEnum SM = new NationalityEnum(_SM);
    public static final NationalityEnum SK = new NationalityEnum(_SK);
    public static final NationalityEnum SI = new NationalityEnum(_SI);
    public static final NationalityEnum SE = new NationalityEnum(_SE);
    public static final NationalityEnum SY = new NationalityEnum(_SY);
    public static final NationalityEnum TN = new NationalityEnum(_TN);
    public static final NationalityEnum TR = new NationalityEnum(_TR);
    public static final NationalityEnum UA = new NationalityEnum(_UA);
    public static final NationalityEnum US = new NationalityEnum(_US);
    public static final NationalityEnum VA = new NationalityEnum(_VA);
    public String getValue() { return _value_;}
    public static NationalityEnum fromValue(String value)
          throws IllegalArgumentException {
        NationalityEnum enumeration = (NationalityEnum)
            _table_.get(value);
        if (enumeration==null) throw new IllegalArgumentException();
        return enumeration;
    }
    public static NationalityEnum fromString(String value)
          throws IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public String toString() { return _value_;}
    public Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NationalityEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/CeyeclopsCentral.Bridge129.Commons.Enums", "NationalityEnum"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
