/**
 * ITransitiService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface ITransitiService extends java.rmi.Remote {
    public String getVersion() throws java.rmi.RemoteException;
    public Integer createTransit(org.datacontract.schemas._2004._07.CeyeclopsCentral_Bridge129_Commons_DTO.TransitNewDTO transit) throws java.rmi.RemoteException;
}
