/**
 * TransitServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class TransitServiceLocator extends org.apache.axis.client.Service implements org.tempuri.TransitService {

    public TransitServiceLocator() {
    }


    public TransitServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TransitServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_ITransitiService
    private String BasicHttpBinding_ITransitiService_address = "http://localhost:8097/soap/BasicHttpBinding_ITransitiService";

    public String getBasicHttpBinding_ITransitiServiceAddress() {
        return BasicHttpBinding_ITransitiService_address;
    }

    // The WSDD service name defaults to the port name.
    private String BasicHttpBinding_ITransitiServiceWSDDServiceName = "BasicHttpBinding_ITransitiService";

    public String getBasicHttpBinding_ITransitiServiceWSDDServiceName() {
        return BasicHttpBinding_ITransitiServiceWSDDServiceName;
    }

    public void setBasicHttpBinding_ITransitiServiceWSDDServiceName(String name) {
        BasicHttpBinding_ITransitiServiceWSDDServiceName = name;
    }

    public org.tempuri.ITransitiService getBasicHttpBinding_ITransitiService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_ITransitiService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_ITransitiService(endpoint);
    }

    public org.tempuri.ITransitiService getBasicHttpBinding_ITransitiService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.BasicHttpBinding_ITransitiServiceStub _stub = new org.tempuri.BasicHttpBinding_ITransitiServiceStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_ITransitiServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_ITransitiServiceEndpointAddress(String address) {
        BasicHttpBinding_ITransitiService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.ITransitiService.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.BasicHttpBinding_ITransitiServiceStub _stub = new org.tempuri.BasicHttpBinding_ITransitiServiceStub(new java.net.URL(BasicHttpBinding_ITransitiService_address), this);
                _stub.setPortName(getBasicHttpBinding_ITransitiServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_ITransitiService".equals(inputPortName)) {
            return getBasicHttpBinding_ITransitiService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "TransitService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "BasicHttpBinding_ITransitiService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {

if ("BasicHttpBinding_ITransitiService".equals(portName)) {
            setBasicHttpBinding_ITransitiServiceEndpointAddress(address);
        }
        else
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
