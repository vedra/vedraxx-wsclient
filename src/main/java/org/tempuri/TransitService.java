/**
 * TransitService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface TransitService extends javax.xml.rpc.Service {
    public String getBasicHttpBinding_ITransitiServiceAddress();

    public org.tempuri.ITransitiService getBasicHttpBinding_ITransitiService() throws javax.xml.rpc.ServiceException;

    public org.tempuri.ITransitiService getBasicHttpBinding_ITransitiService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
