/**
 * CeyeclopsEvents.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ceyeclopsresults;

public interface CeyeclopsEvents extends javax.xml.rpc.Service {
    public java.lang.String getCeyeclopsEventsPortAddress();

    public ceyeclopsresults.CeyeclopsEventsPortType getCeyeclopsEventsPort() throws javax.xml.rpc.ServiceException;

    public ceyeclopsresults.CeyeclopsEventsPortType getCeyeclopsEventsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
