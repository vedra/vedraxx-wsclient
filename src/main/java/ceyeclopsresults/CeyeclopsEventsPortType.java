/**
 * CeyeclopsEventsPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ceyeclopsresults;

public interface CeyeclopsEventsPortType extends java.rmi.Remote {
    public java.lang.String insert_event(ceyeclopsresults.Evento event) throws java.rmi.RemoteException;
}
