/**
 * Evento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ceyeclopsresults;

public class Evento  implements java.io.Serializable {
    private java.lang.String etime;

    private java.lang.String plate;

    private java.lang.String pconfidence;

    private java.lang.String vctype;

    private java.lang.String vconfidence;

    private java.lang.String imgname;

    private java.lang.String gdtype;

    private java.lang.String cname;

    private java.lang.String cnum;

    private java.lang.String m_list;

    private java.lang.String m_driver;

    private java.lang.String prov;

    private byte[] jpgdata;

    public Evento() {
    }

    public Evento(
           java.lang.String etime,
           java.lang.String plate,
           java.lang.String pconfidence,
           java.lang.String vctype,
           java.lang.String vconfidence,
           java.lang.String imgname,
           java.lang.String gdtype,
           java.lang.String cname,
           java.lang.String cnum,
           java.lang.String m_list,
           java.lang.String m_driver,
           java.lang.String prov,
           byte[] jpgdata) {
           this.etime = etime;
           this.plate = plate;
           this.pconfidence = pconfidence;
           this.vctype = vctype;
           this.vconfidence = vconfidence;
           this.imgname = imgname;
           this.gdtype = gdtype;
           this.cname = cname;
           this.cnum = cnum;
           this.m_list = m_list;
           this.m_driver = m_driver;
           this.prov = prov;
           this.jpgdata = jpgdata;
    }


    /**
     * Gets the etime value for this Evento.
     * 
     * @return etime
     */
    public java.lang.String getEtime() {
        return etime;
    }


    /**
     * Sets the etime value for this Evento.
     * 
     * @param etime
     */
    public void setEtime(java.lang.String etime) {
        this.etime = etime;
    }


    /**
     * Gets the plate value for this Evento.
     * 
     * @return plate
     */
    public java.lang.String getPlate() {
        return plate;
    }


    /**
     * Sets the plate value for this Evento.
     * 
     * @param plate
     */
    public void setPlate(java.lang.String plate) {
        this.plate = plate;
    }


    /**
     * Gets the pconfidence value for this Evento.
     * 
     * @return pconfidence
     */
    public java.lang.String getPconfidence() {
        return pconfidence;
    }


    /**
     * Sets the pconfidence value for this Evento.
     * 
     * @param pconfidence
     */
    public void setPconfidence(java.lang.String pconfidence) {
        this.pconfidence = pconfidence;
    }


    /**
     * Gets the vctype value for this Evento.
     * 
     * @return vctype
     */
    public java.lang.String getVctype() {
        return vctype;
    }


    /**
     * Sets the vctype value for this Evento.
     * 
     * @param vctype
     */
    public void setVctype(java.lang.String vctype) {
        this.vctype = vctype;
    }


    /**
     * Gets the vconfidence value for this Evento.
     * 
     * @return vconfidence
     */
    public java.lang.String getVconfidence() {
        return vconfidence;
    }


    /**
     * Sets the vconfidence value for this Evento.
     * 
     * @param vconfidence
     */
    public void setVconfidence(java.lang.String vconfidence) {
        this.vconfidence = vconfidence;
    }


    /**
     * Gets the imgname value for this Evento.
     * 
     * @return imgname
     */
    public java.lang.String getImgname() {
        return imgname;
    }


    /**
     * Sets the imgname value for this Evento.
     * 
     * @param imgname
     */
    public void setImgname(java.lang.String imgname) {
        this.imgname = imgname;
    }


    /**
     * Gets the gdtype value for this Evento.
     * 
     * @return gdtype
     */
    public java.lang.String getGdtype() {
        return gdtype;
    }


    /**
     * Sets the gdtype value for this Evento.
     * 
     * @param gdtype
     */
    public void setGdtype(java.lang.String gdtype) {
        this.gdtype = gdtype;
    }


    /**
     * Gets the cname value for this Evento.
     * 
     * @return cname
     */
    public java.lang.String getCname() {
        return cname;
    }


    /**
     * Sets the cname value for this Evento.
     * 
     * @param cname
     */
    public void setCname(java.lang.String cname) {
        this.cname = cname;
    }


    /**
     * Gets the cnum value for this Evento.
     * 
     * @return cnum
     */
    public java.lang.String getCnum() {
        return cnum;
    }


    /**
     * Sets the cnum value for this Evento.
     * 
     * @param cnum
     */
    public void setCnum(java.lang.String cnum) {
        this.cnum = cnum;
    }


    /**
     * Gets the m_list value for this Evento.
     * 
     * @return m_list
     */
    public java.lang.String getM_list() {
        return m_list;
    }


    /**
     * Sets the m_list value for this Evento.
     * 
     * @param m_list
     */
    public void setM_list(java.lang.String m_list) {
        this.m_list = m_list;
    }


    /**
     * Gets the m_driver value for this Evento.
     * 
     * @return m_driver
     */
    public java.lang.String getM_driver() {
        return m_driver;
    }


    /**
     * Sets the m_driver value for this Evento.
     * 
     * @param m_driver
     */
    public void setM_driver(java.lang.String m_driver) {
        this.m_driver = m_driver;
    }


    /**
     * Gets the prov value for this Evento.
     * 
     * @return prov
     */
    public java.lang.String getProv() {
        return prov;
    }


    /**
     * Sets the prov value for this Evento.
     * 
     * @param prov
     */
    public void setProv(java.lang.String prov) {
        this.prov = prov;
    }


    /**
     * Gets the jpgdata value for this Evento.
     * 
     * @return jpgdata
     */
    public byte[] getJpgdata() {
        return jpgdata;
    }


    /**
     * Sets the jpgdata value for this Evento.
     * 
     * @param jpgdata
     */
    public void setJpgdata(byte[] jpgdata) {
        this.jpgdata = jpgdata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Evento)) return false;
        Evento other = (Evento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.etime==null && other.getEtime()==null) || 
             (this.etime!=null &&
              this.etime.equals(other.getEtime()))) &&
            ((this.plate==null && other.getPlate()==null) || 
             (this.plate!=null &&
              this.plate.equals(other.getPlate()))) &&
            ((this.pconfidence==null && other.getPconfidence()==null) || 
             (this.pconfidence!=null &&
              this.pconfidence.equals(other.getPconfidence()))) &&
            ((this.vctype==null && other.getVctype()==null) || 
             (this.vctype!=null &&
              this.vctype.equals(other.getVctype()))) &&
            ((this.vconfidence==null && other.getVconfidence()==null) || 
             (this.vconfidence!=null &&
              this.vconfidence.equals(other.getVconfidence()))) &&
            ((this.imgname==null && other.getImgname()==null) || 
             (this.imgname!=null &&
              this.imgname.equals(other.getImgname()))) &&
            ((this.gdtype==null && other.getGdtype()==null) || 
             (this.gdtype!=null &&
              this.gdtype.equals(other.getGdtype()))) &&
            ((this.cname==null && other.getCname()==null) || 
             (this.cname!=null &&
              this.cname.equals(other.getCname()))) &&
            ((this.cnum==null && other.getCnum()==null) || 
             (this.cnum!=null &&
              this.cnum.equals(other.getCnum()))) &&
            ((this.m_list==null && other.getM_list()==null) || 
             (this.m_list!=null &&
              this.m_list.equals(other.getM_list()))) &&
            ((this.m_driver==null && other.getM_driver()==null) || 
             (this.m_driver!=null &&
              this.m_driver.equals(other.getM_driver()))) &&
            ((this.prov==null && other.getProv()==null) || 
             (this.prov!=null &&
              this.prov.equals(other.getProv()))) &&
            ((this.jpgdata==null && other.getJpgdata()==null) || 
             (this.jpgdata!=null &&
              java.util.Arrays.equals(this.jpgdata, other.getJpgdata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEtime() != null) {
            _hashCode += getEtime().hashCode();
        }
        if (getPlate() != null) {
            _hashCode += getPlate().hashCode();
        }
        if (getPconfidence() != null) {
            _hashCode += getPconfidence().hashCode();
        }
        if (getVctype() != null) {
            _hashCode += getVctype().hashCode();
        }
        if (getVconfidence() != null) {
            _hashCode += getVconfidence().hashCode();
        }
        if (getImgname() != null) {
            _hashCode += getImgname().hashCode();
        }
        if (getGdtype() != null) {
            _hashCode += getGdtype().hashCode();
        }
        if (getCname() != null) {
            _hashCode += getCname().hashCode();
        }
        if (getCnum() != null) {
            _hashCode += getCnum().hashCode();
        }
        if (getM_list() != null) {
            _hashCode += getM_list().hashCode();
        }
        if (getM_driver() != null) {
            _hashCode += getM_driver().hashCode();
        }
        if (getProv() != null) {
            _hashCode += getProv().hashCode();
        }
        if (getJpgdata() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getJpgdata());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getJpgdata(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Evento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:ceyeclopsresults", "evento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("etime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "etime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("plate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "plate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pconfidence");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pconfidence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vctype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vctype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vconfidence");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vconfidence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imgname");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imgname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gdtype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "gdtype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cname");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cnum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cnum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("m_list");
        elemField.setXmlName(new javax.xml.namespace.QName("", "m_list"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("m_driver");
        elemField.setXmlName(new javax.xml.namespace.QName("", "m_driver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prov");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prov"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("jpgdata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "jpgdata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "hexBinary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
